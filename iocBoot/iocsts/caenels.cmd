#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## test
#epicsEnvSet("DEVIP","127.0.0.1")
#epicsEnvSet("DEVIP","172.17.10.27")
#epicsEnvSet("IOCBL","PINK")
#epicsEnvSet("IOCDEV","CAE1")

## Macros
epicsEnvSet("PREFIX",    "$(IOCBL):")
epicsEnvSet("RECORD",    "$(IOCDEV):")
epicsEnvSet("ASYNPORT",  "cae")
epicsEnvSet("PORT",      "CAE")
epicsEnvSet("TEMPLATE",  "TetrAMM")
epicsEnvSet("QSIZE",     "20")
epicsEnvSet("RING_SIZE", "20000")
epicsEnvSet("TSPOINTS",  "20000")

epicsEnvSet("DEVPORT","10001")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "2000000")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db:$(QUADEM)/db")

## Config
drvAsynIPPortConfigure("$(ASYNPORT)", "$(DEVIP):$(DEVPORT)", 0, 0, 0)
asynOctetSetInputEos("$(ASYNPORT)",  0, "\r\n")
asynOctetSetOutputEos("$(ASYNPORT)", 0, "\r")

drvTetrAMMConfigure("$(PORT)", "$(ASYNPORT)", $(RING_SIZE))

## Load record instances
dbLoadRecords("$(QUADEM)/db/$(TEMPLATE).template", "P=$(PREFIX), R=$(RECORD), PORT=$(PORT)")
< $(QUADEM)/iocBoot/commonPlugins.cmd
< ${TOP}/iocBoot/${IOC}/extra-plugins.cmd

cd "${TOP}/iocBoot/${IOC}"

## Autosave
set_requestfile_path("./")
set_requestfile_path("$(QUADEM)/quadEMApp/Db")
set_requestfile_path("$(ADCORE)/ADApp/Db")

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## Autosave - save settings every thirty seconds
create_monitor_set("auto_settings.req", 30, "P=$(IOCBL):,R=$(IOCDEV):")

## Start any sequence programs
#seq sncxxx,"user=epics"
