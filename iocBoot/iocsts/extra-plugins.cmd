# This creates a waveform large enough for 11x10000 arrays.
# image2
NDStdArraysConfigure("$(PORT)_Image2", $(QSIZE), 0, "$(PORT)", 11)
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=$(RECORD)image2:,PORT=$(PORT)_Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=11,TYPE=Float64,FTVL=DOUBLE,NELEMENTS=110000,ENABLED=0")

# image3
NDStdArraysConfigure("$(PORT)_Image3", $(QSIZE), 0, "$(PORT)", 11)
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=$(RECORD)image3:,PORT=$(PORT)_Image3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=11,TYPE=Float64,FTVL=DOUBLE,NELEMENTS=110000,ENABLED=0")

# image4
NDStdArraysConfigure("$(PORT)_Image4", $(QSIZE), 0, "$(PORT)", 11)
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=$(RECORD)image4:,PORT=$(PORT)_Image4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=11,TYPE=Float64,FTVL=DOUBLE,NELEMENTS=110000,ENABLED=0")



